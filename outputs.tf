output "subnets" {
  value = aws_subnet.private.*.id
}

output "subnet_cidrs" {
  value = aws_subnet.private.*.cidr_block
}
